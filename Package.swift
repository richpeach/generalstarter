// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GeneralStarter",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "GeneralStarter",
            targets: ["GeneralStarter"]),
    ],
    dependencies: [
        .package(url: "https://github.com/adaptyteam/AdaptySDK-iOS.git", from: "2.9.2"),
        .package(url: "https://github.com/AndreVasilev/AVVPNService.git", from: "0.1.4")
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "GeneralStarter", dependencies: [
                .product(name: "Adapty", package: "AdaptySDK-iOS"),
                .product(name: "AVVPNService", package: "AVVPNService"),
            ]),
        .testTarget(
            name: "GeneralStarterTests",
            dependencies: ["GeneralStarter"]),
    ]
)
