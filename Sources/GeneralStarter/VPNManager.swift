//
//  VPNManager.swift
//
//  Created by Evgeniy Bruchkovskiy.
//

import Foundation
import AVVPNService
import NetworkExtension

public struct VPNCredential {
    public let title: String
    public let server: String
    public let username: String
    public let password: String
    public let shared: String
    
    public init(title: String, server: String, username: String, password: String, shared: String) {
        self.title = title
        self.server = server
        self.username = username
        self.password = password
        self.shared = shared
    }
}

private extension VPNCredential {
    var toAVVPNCred: AVVPNCredentials {
        AVVPNCredentials.IPSec(title: title, server: server, username: username, password: password, shared: shared)
    }
}

public enum ConnectStatus {
    case disconnect, connection, connected
}

final public class VPNManager {
    public enum Status: Int {
        case invalid = 0
        case disconnected = 1
        case connecting = 2
        case connected = 3
        case reasserting = 4
        case disconnecting = 5
        case unknown
    }
    
    private let vpnManager = NEVPNManager.shared()
    private var statusCompletion: ((Status) -> Void)?
    
    public init() {}
    
    public func connectTo(_ cred: VPNCredential, completion: @escaping (Error?) -> Void) {
        AVVPNService.shared.connect(credentials: cred.toAVVPNCred, completion)
    }
    
    public func disconnect() {
        AVVPNService.shared.disconnect()
    }
    
    public func subscribeOnStatus(completion: @escaping (Status) -> Void) {
        statusCompletion = completion
        if vpnManager.protocolConfiguration == nil {
            vpnManager.loadFromPreferences { _ in
                completion(self.vpnManager.connection.status.status)
            }
        } else {
            completion(vpnManager.connection.status.status)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeStatus(_:)), name: NSNotification.Name.NEVPNStatusDidChange, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

private extension VPNManager {
    @objc func didChangeStatus(_ notification: Notification) {
        if let connection = notification.object as? NEVPNConnection {
            statusCompletion?(connection.status.status)
        }
    }
}

private extension NEVPNStatus {
    var status: VPNManager.Status {
        VPNManager.Status(rawValue: self.rawValue) ?? .unknown
    }
}
