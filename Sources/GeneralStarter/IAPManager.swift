//
//  Created by Evgeniy Bruchkovskiy.
//

import Adapty
import StoreKit
import Foundation

public struct IAPProduct {
    /// Unique identifier of a product from App Store Connect
    public let vendorProductId: String
    /// Underlying system representation of the product.
    public let skProduct: SKProduct
    /// The cost of the product in the local currency.
    public var price: Decimal
    /// The currency code of the locale used to format the price of the product.
    public var currencyCode: String?
    /// The currency symbol of the locale used to format the price of the product.
    public var currencySymbol: String? { skProduct.priceLocale.currencySymbol }
    /// The region code of the locale used to format the price of the product.
    public var regionCode: String? { skProduct.priceLocale.regionCode }
    /// The price's language is determined by the preferred language set on the device.
    public var localizedPrice: String?
    /// The period's language is determined by the preferred language set on the device.
    public var localizedSubscriptionPeriod: String?
}

final public class IAPManager {
    public enum IAPError: Error, LocalizedError {
        case productIsEmpty, unowned
        
        public var errorDescription: String? {
            switch self {
            case .productIsEmpty:
                return "Product is empty"
            case .unowned:
                return "Unknown"
            }
        }
    }
    
    public enum SubscriptionStatus {
        case subscribed, unknown
    }
    
    private init() {}
    
    static public let shared: IAPManager = IAPManager()
    
    private var info: [String:[AdaptyPaywallProduct]]?
    
    public func activate(apiKey: String) {
        Adapty.activate(apiKey)
    }
    
    public func getProducts(force: Bool = false, placementId: String, productIds: [String], completion: (([IAPProduct]?, Error?) -> Void)? = nil) {
        if let info, let paywallProducts = info[placementId], force == false {
            completion?(paywallProducts.toIAPProducts(), nil)
        } else {
            Adapty.getPaywall(placementId: placementId, locale: Locale.current.identifier) { [weak self] result in
                switch result {
                case .success(let paywall):
                    Adapty.getPaywallProducts(paywall: paywall) { [weak self] result in
                        switch result {
                        case let .success(products):
                            self?.addToInfo(placementId: placementId, products: products)
                            completion?(products.toIAPProducts(), nil)
                        case let .failure(error):
                            completion?(nil, error)
                        }
                    }
                case .failure(let error):
                    completion?(nil, error)
                }
            }
        }
    }

    public func purchase(vendorProductId: String, completion: @escaping (Error?) -> Void) {
        if let info, let product = (info.map { $0.value }.flatMap { $0 }.first(where: { $0.vendorProductId == vendorProductId })) {
            Adapty.makePurchase(product: product) { result in
                switch result {
                case .success: completion(nil)
                case .failure(let error): completion(error)
                }
            }
        } else {
            completion(IAPError.unowned)
        }
    }
    
    public func restorePurchase(completion: @escaping (Error?) -> Void) {
        Adapty.restorePurchases { result in
            switch result {
            case .success: completion(nil)
            case .failure(let error): completion(error)
            }
        }
    }
    
    public func subscriptionStatus(completion: @escaping (SubscriptionStatus) -> Void) {
        Adapty.getProfile { result in
            if let profile = try? result.get() {
                let isActive = profile.accessLevels["premium"]?.isActive ?? false
                completion(isActive ? .subscribed : .unknown)
            } else {
                completion(.unknown)
            }
        }
    }
    
    // MARK: - Private
    private func addToInfo(placementId: String, products: [AdaptyPaywallProduct]) {
        if self.info != nil {
            info![placementId] = products
        } else {
            self.info = [placementId:products]
        }
    }
}

public extension IAPProduct {
    func priceOffer(formatter: NumberFormatter? = nil, devide: Float = 0) -> String? {
        let skProduct = self.skProduct
        let price = Float(truncating: skProduct.price) / devide
        
        var usingFormatter: NumberFormatter!
        
        if let formatter {
            usingFormatter = formatter
        } else {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = skProduct.priceLocale
        }
        return devide != 0 ? usingFormatter.string(from: NSNumber(value: price)) : usingFormatter.string(from: skProduct.price)
    }
}

extension Array where Element == AdaptyPaywallProduct {
    func toIAPProducts() -> [IAPProduct] {
        self.map { IAPProduct(vendorProductId: $0.vendorProductId, skProduct: $0.skProduct, price: $0.price, currencyCode: $0.currencyCode, localizedPrice: $0.localizedPrice, localizedSubscriptionPeriod: $0.localizedSubscriptionPeriod) }
    }
}
